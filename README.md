
Source code for Edge-based Hyperdimensional Learning System with Brain-Like Neural Adaptation

NeuralHD is a new Hyperdimensional Computing Approach with a dynamic encoder for adaptive learning. Inspired by human neuronal regeneration study in neuroscience, NeuralHD identifies insignificant dimensions and regenerates those dimensions to enhance the learning capability and  robustness. 

### Usage ####

1. Check in Config.py that the data_location is pointing to the directory of the datasets
2. run jupyter notebook "Paper_Ready.ipynb"
